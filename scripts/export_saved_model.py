from tensorflow import keras

model = keras.models.load_model('./AlexNetModel.hdf5')

path_to_saved_model = '../my_image_classifier/1'

# Saving the keras model in SavedModel format
keras.experimental.export_saved_model(model, path_to_saved_model)

#restored_saved_model = keras.experimental.load_from_saved_model(path_to_saved_model)