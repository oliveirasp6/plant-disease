# Plant Disease

Classificação de plantas saudáveis e doentes.

![plant class](assets/class.jpeg "classes")

Os seguintes tópicos são abordados:

1. Construindo uma rede neural convolucional utilizando TensorFlow 2.0
2. Implantando o modelo usando o TensorFlow Serving

### Utilização do modelo

Clone o repositório:

`git clone https://gitlab.com/oliveirasp6/plant-disease.git`

Inicie o TensorFlow Serving. Para iniciar o servidor TensorFlow Serving em sua máquina local, execute o seguinte comando:

`tensorflow_model_server --model_base_path=/home/lucas/Documents/Projetos/plant_disease/my_image_classifier/ --rest_api_port=9000 --model_name=ImageClassifier`

`--model_base_path`: Este deve ser um caminho absoluto; caso contrário, você receberá um erro dizendo:

`Failed to start server. Error: Invalid argument: Expected model ImageClassifier to have an absolute path or URI; got base_path()=./my_image_classifier`

Dentro do diretório **scripts** execute:

`python serving_sample_request.py`

Consulte esse [artigo](https://medium.com/@himanshurawlani/getting-started-with-tensorflow-2-0-faf5428febae) para obter uma explicação detalhada do projeto.